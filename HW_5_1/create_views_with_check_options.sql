Use V_I_module_3
go

--- create views whith check_options

CREATE OR ALTER VIEW [guest_nane_Volodymyr] 
(
       [guest_name]
      ,[arrival_date]
      ,[departure_time]
      ,[guest_email]
	  ,[guest_phone_number]
	  ,[quantity_children]
  )
  AS
  SELECT 
       [name]
      ,[arrival_date]
      ,[departure_time]
      ,[email]
	  ,[phone_number]
	  ,[children]
  FROM [dbo].[guest]
  where [name] like '%Volodymyr'
  with check option
go

CREATE OR ALTER VIEW [room_suite] 
(
       [room_number]
      ,[room_type]
      ,[quantity_balcony]
      ,[quantity_bed]
	  ,[quantity_extra_bed]
	  ,[comment]
  )
  AS
  SELECT 
       [number]
      ,[type]
      ,[balcony]
      ,[bed]
	  ,[extra_bed]
	  ,[comment]
  FROM [dbo].[room]
  where [type] like 'suite'
  with check option
go

select * from [guest_nane_Volodymyr] 
go

select * from [room_suite] 
go