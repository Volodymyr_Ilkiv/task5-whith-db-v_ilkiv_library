use V_Ilkiv_Library
go

drop trigger if exists [dbo].[tr_Authors_U] 
go

drop trigger if exists [dbo].[tr_Books_U] 
go

drop trigger if exists [dbo].[tr_BooksAuthors_U] 
go

drop trigger if exists [dbo].[tr_Publishers_U] 
go

-- UPDATE TRIGGERS

create or alter trigger [dbo].[tr_Authors_U]  on [dbo].[Authors]
after update
as
begin
declare @update_user varchar(7) = SYSTEM_USER
declare @update_date datetime = GETDATE()
update [dbo].[Authors]
set [dbo].[Authors].[updated]=@update_date, [dbo].[Authors].[updated_by]=@update_user
where [dbo].[Authors].[Author_Id]=(select [Author_Id] from INSERTED)
end 
go

create or alter trigger [dbo].[tr_Books_U]  on [dbo].[Books]
after update
as
begin
declare @update_user varchar(7) = SYSTEM_USER
declare @update_date datetime = GETDATE()
update [dbo].[Books]
set [dbo].[Books].[updated]=@update_date, [dbo].[Books].[updated_by]=@update_user
where [dbo].[Books].[ISBN]=(select [ISBN] from INSERTED)
end 
go

create or alter trigger [dbo].[tr_BooksAuthors_U]  on [dbo].[BooksAuthors]
after update
as
begin
declare @update_user varchar(7) = SYSTEM_USER
declare @update_date datetime = GETDATE()
update [dbo].[BooksAuthors]
set [dbo].[BooksAuthors].[updated]=@update_date, [dbo].[BooksAuthors].[updated_by]=@update_user
where [dbo].[BooksAuthors].[BooksAuthors_id]=(select [BooksAuthors_id] from INSERTED)
end 
go

create or alter trigger [dbo].[tr_Publishers_U]  on [dbo].[Publishers]
after update
as
begin
declare @update_user varchar(7) = SYSTEM_USER
declare @update_date datetime = GETDATE()
update [dbo].[Publishers]
set [dbo].[Publishers].[updated]=@update_date, [dbo].[Publishers].[updated_by]=@update_user
where [dbo].[Publishers].[Publisher_Id]=(select [Publisher_Id] from INSERTED)
end 
go