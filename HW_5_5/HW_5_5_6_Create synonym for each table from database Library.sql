use V_Ilkiv_Library_synonym
go

drop synonym if exists [syn].[Authors]
go
drop synonym if exists [syn].[Authors_log]
go
drop synonym if exists [syn].[Books]
go
drop synonym if exists [syn].[BooksAuthors]
go
drop synonym if exists [syn].[Publishers]
go

drop schema if exists [syn]
go
create schema [syn]
go


create synonym [syn].[Authors] for [V_Ilkiv_Library].[dbo].[Authors]
go
create synonym [syn].[Authors_log] for [V_Ilkiv_Library].[dbo].[Authors_log]
go
create synonym [syn].[Books] for [V_Ilkiv_Library].[dbo].[Books]
go
create synonym [syn].[BooksAuthors] for [V_Ilkiv_Library].[dbo].[BooksAuthors]
go
create synonym [syn].Publishers for [V_Ilkiv_Library].[dbo].Publishers
go
