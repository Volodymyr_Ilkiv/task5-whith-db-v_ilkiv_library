use V_I_module_3
go
drop trigger if exists [mod].[tr_insert] 
go
drop trigger if exists [mod].[tr_update]
go
drop trigger if exists [mod].[tr_delete]  
go

-- INSERT TRIGGER

create or alter trigger [mod].[tr_insert] on [mod].[produckt]
after insert
as
begin
declare @operation varchar(7) = 'INSERT'
declare @update_date datetime = GETDATE()
insert into [mod].[produckt_operation](
[produckt_id], [name], [size], [quantity], [price], 
[total_price], [provider], [provider_phone], [provideer_email], 
[price_in_valute], [manufacturer], [country_manufacturer], [sold], 
[tax_type], [tax_rare], [sum_tax_rare], [date_operation], [type_operation] 
)
select *,@update_date, @operation from INSERTED
end 
go

-- UPDATE TRIGGER

create or alter trigger [mod].[tr_update]
on [mod].[produckt]
after update
as
begin
declare @operation varchar(7) = 'UPDATE'
declare @update_date datetime = GETDATE()
insert into [trig].[produckt_operation](
[produckt_id], [name], [size], [quantity], [price], 
[total_price], [provider], [provider_phone], [provideer_email], 
[price_in_valute], [manufacturer], [country_manufacturer], [sold], 
[tax_type], [tax_rare], [sum_tax_rare], [date_operation], [type_operation]   
)
select *, @update_date, @operation from INSERTED
end
go

-- DELETE TRIGGER

create or alter trigger [mod].[tr_delete] 
on [mod].[produckt]
for delete 
as
begin
declare @operation varchar(7) = 'DELETE'
declare @update_date datetime = GETDATE()
insert into [mod].[produckt_operation](
[produckt_id], [name], [size], [quantity], [price], 
[total_price], [provider], [provider_phone], [provideer_email], 
[price_in_valute], [manufacturer], [country_manufacturer], [sold], 
[tax_type], [tax_rare], [sum_tax_rare], [date_operation], [type_operation]    
)
select *, @update_date, @operation from DELETED
end
go