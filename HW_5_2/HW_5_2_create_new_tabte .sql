use V_I_module_3
go

drop table if exists [mod].[produckt]
drop table if exists [mod].[produckt_operation]
go
drop schema if exists [mod]
go
create schema [mod]
go


create table [mod].[produckt](
[id] int not null IDENTITY (1 , 1),  
[name] varchar (250),
[size] varchar (50),
[quantity] int,
[price] decimal(20,4),
[total_price] decimal(20,4),
[provider] varchar (250),
[provider_phone] varchar (50),
[provideer_email] varchar (50),
[price_in_valute] decimal(20,4),
[manufacturer] varchar (250),
[country_manufacturer] varchar (250),
[sold] bit,
[tax_type] bit,
[tax_rare] decimal(20,4),
[sum_tax_rare] decimal(20,4)
constraint pk_produckt primary key (id)
)
go 
create table [mod].[produckt_operation](
[id] int not null IDENTITY (1 , 1), 
[produckt_id] int,
[name] varchar (250),
[size] varchar (50),
[quantity] int,
[price] decimal(20,4),
[total_price] decimal(20,4),
[provider] varchar (250),
[provider_phone] varchar (50),
[provideer_email] varchar (50),
[price_in_valute] decimal(20,4),
[manufacturer] varchar (250),
[country_manufacturer] varchar (250),
[sold] bit,
[tax_type] bit,
[tax_rare] decimal(20,4),
[sum_tax_rare] decimal(20,4),
[date_operation] datetime,
[type_operation] varchar(7)
constraint pk_produckt_operation primary key (id)
)
go